/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;

/**
 *
 * @author JcMoutoh
 */
public enum RequestEnum {
    ADDONDATABUNDLE,NORMALDATABUNDLE,NORENEWAL,
    MOMOBONUSDATA,MOMOPASSJOUR,MOMOPASS48H,AUTORENEWAL,
    MOMOPASSWEEK,MOMOPASSMOIS,PASSENCOURS,
    PASSENATTENTE,DECOUVRIR,CONFIRMER,ANNULER,
    PROMO,PAYG,MONCOMPTE,DEACTIVATE,ACTIVATE;
    
}
