/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles.momo;
import accesspostgresdb.data.DataClient;
import mtngc.databundles.*;
import mtngc.databundles.momo.SAXHandler;
import mtngc.databundles.momo.MoMoResponse;
import mtngc.databundles.momo.MoMoRequestStore;
import dbaccess.dbsrver.datareseller.DataDA;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse ;
//import javax.servlet.http.HttpSession ;
import javax.servlet.ServletContext ;
import java.io.*;
import java.util.*;
import mdpclient.MDPClientEngine;
import mdpclient.MDPSubscriptionResponse;
import mtngc.ussd.AbstractBundleSession;
import mtngc.ussd.AbstractMainMenu;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import mtngc.ussd.AbstractUSSDHandler;
//import mtngc.bundles.core.*;
import dbaccess.dbsrver.datareseller.DataPackage;

import javax.xml.parsers.*;
import mtngc.databundles.BundleSession;
import mtngc.databundles.USSDPush;
import mtngc.databundles.RequestEnum;
import mtngc.databundles.ResponseEnum;
import org.xml.sax.*;
import ucipclient.*;

/**
 *
 * @author JcMoutoh
 */
public class MoMoPaymentHandler extends AbstractUSSDHandler {
    static String externalData1;
    boolean isAddon;  
    String refillExternalData = "DATABUNDLE";
    public MoMoPaymentHandler(){
    }
    
        static
    {  
        ContextKeyName = "data_ussd";
        externalData1 = ContextKeyName;
        supportedMSISDNs = new ArrayList <Integer> ();
        String filePath = "C:\\Logs\\"+externalData1+"_v1";
        try{
        File file = new File(filePath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        MLogger.setHomeDirectory(filePath);
        
        //MLogger.setHomeDirectory("C:\\Logs\\"+ContextKeyName);
    }
        
    private String handleRequest(MoMoResponse momoResp,boolean provision){
        String str = null;
        DataClient client = new DataClient();
        MoMoRequestStore requestStore = MoMoRequestStore.getInstance();
        String transactionId = momoResp.getTransactionId();
        MoMoRequest momoReq = requestStore.fetch(transactionId);
        if(momoReq != null){
            String msisdn = momoReq.getMsisdn();
            DataPackage option = momoReq.getOption();
            DataProvisioningEngine provEngine = new DataProvisioningEngine();
            UCIPClientEngine ucipEngine = new UCIPClientEngine();
            MLogger.Log(this, LogLevel.DEBUG, "TransactionId:"+transactionId+"|Subscriber:"+msisdn+"|Transaction successfull on Mobile Money");
           
            if(provision){  
               UCIPUpdateRefillProfilIDResponse refillDAResponse = new UCIPUpdateRefillProfilIDResponse();;
               if(momoReq.getisAddon()){
                 refillDAResponse = ucipEngine.RefillProfileID(msisdn, transactionId,  option.getAddonkeywordmomo(), refillExternalData);
                 MLogger.Log(this, LogLevel.DEBUG, "TransactionId:"+transactionId+"|Subscriber:"+msisdn+"|Sending UCIP Provisioning request |RefilId:"+option.getAddonkeywordmomo());
                 client.insertTransactionDetails( msisdn,transactionId, option.getPackageType(), option.getPackageName(), option.getDuration(),null, option.getPrice(), option.getAddonkeywordmomo(), momoResp.getStatusDesc(), refillExternalData, "NO",null);
               }else 
               
                  refillDAResponse = ucipEngine.RefillProfileID(msisdn, transactionId,  option.getKeywordMoMo(), refillExternalData);
                MLogger.Log(this, LogLevel.DEBUG, "TransactionId:"+transactionId+"|Subscriber:"+msisdn+"|Sending UCIP Provisioning request |RefilId:"+option.getKeywordMoMo());
               //str = "Test"; 
                client.insertTransactionDetails(transactionId, msisdn, option.getPackageType(), option.getPackageName(), option.getDuration(), null,option.getPrice(), option.getKeyword(), momoResp.getStatusDesc(), refillExternalData, "NO",null);
                requestStore.remove(transactionId);
            }else{

                
                String str1 = momoResp.getStatusDesc();
                MLogger.Log(this, LogLevel.DEBUG, "TransactionId:"+transactionId+"|Subscriber:"+msisdn+"|ECW has returned the following error "+str1);
                
                if(str1.equals("TARGET_AUTHORIZATION_ERROR")){
                    
                    if(momoReq.getisAddon()){
                      str = "Solde insuffisant pour acheter "+option.getPackageName()+" a " + option.getAddonprice()+" GNF";  
                        
                    }else 
                    
                    str = "Solde insuffisant pour acheter "+option.getPackageName()+" a " + option.getPrice()+" GNF";
                }else if(str1.equals("ACCOUNT_NOT_FOUND")){
                    str = "Vous n'avez pas de compte MoMo. S'il vous plaît Veuillez contacter le service client au 111.";
                }else {
                    str = "Cette transaction ne peut etre effectuee, Veuillez contacter le service client au 111.";
                }
                
            }
            MLogger.Log(this, LogLevel.DEBUG, "TransactionId:"+transactionId+"|Subscriber:"+msisdn+"|Sending USSD PUSH Message "+str);
                
            String ussdCode = momoReq.getUSSDCode();
            USSDPush push = new USSDPush();
            push.push(msisdn, str, ussdCode);

        }else {
            str = "Transaction successfull on Mobile Money but failed to find the original Mobile Money request. Can not be provisioned!!!!.";
            MLogger.Log(this, LogLevel.DEBUG, "TransactionId:"+transactionId+"|"+str);
        }
        
        return str;
    }
    
    
    
    public void handleRequest(HttpServletRequest request, HttpServletResponse response){
        //String str1 = request.getQueryString();
        //Object obj = request.getParameterNames();
        TraceParametersAndHeaders(request);
        StringBuilder sb = new StringBuilder();
        try {
            MLogger.Log(this, LogLevel.DEBUG, "MoMo Response recieved. Fetching request body");
                    
            MoMoResponse momoResp =  getMoMoResponse(request);
            boolean mustProvision = false;
            if(momoResp != null){
                String statusCode = momoResp.getStatusCode();
                String transactionId = momoResp.getTransactionId();
                if(statusCode.equals("01")){
                    String str = this.handleRequest(momoResp, true);
                    sb.append(str);
                }else {
                    String str = this.handleRequest(momoResp, false);
                    sb.append(str);
                }           
            }else{
                String str = "Failed to process MoMo request.";
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, str);
            }
            
            response.setContentType("UTF-8");
            response.setHeader("Freeflow", "FC");
            response.setHeader("cpRefId", "emk2545");
            
            PrintWriter out = response.getWriter();
//  
            out.append(sb.toString());
            MLogger.Log(this, LogLevel.ALL, sb.toString());
            out.close();
        } catch (Exception e) {
            MLogger.Log(this, e);
        }
            
        
    }
    
    
    
    protected AbstractBundleSession CreateBundleSession(){
        return (AbstractBundleSession)new BundleSession();
    }
    protected AbstractMainMenu CreateMainMenu(String msisdn){
        return null;
    }
    
    private MoMoResponse getMoMoResponse(HttpServletRequest request)throws IOException,SAXException, ParserConfigurationException {
        
        String soapPayload = getBody(request);
        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
	SAXParser parser = parserFactor.newSAXParser();
	SAXHandler handler = new SAXHandler();
    
        parser.parse(new ByteArrayInputStream(soapPayload.getBytes("utf-8")), handler);
        return handler.getMoMoResponse();
    }   
      
    private  String getBody(HttpServletRequest request) throws IOException {

        String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                stringBuilder.append("");
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw ex;
                }
            }
        }

        body = stringBuilder.toString();
        return body;
    }
     
    public boolean isIsAddon() {
        return isAddon;
    }

    public void setIsAddon(boolean isAddon) {
        this.isAddon = isAddon;
    }
    
}
