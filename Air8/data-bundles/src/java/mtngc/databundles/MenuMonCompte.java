/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;

import java.util.List;

/**
 *
 * @author Administrateur
 */
public class MenuMonCompte {
    
     private RequestEnum requestEnum;
       public MenuMonCompte(RequestEnum requestEnum){
        this.requestEnum=requestEnum;
    }
    
    String header = "Menu mon Compte Data";
    
    public String getString(){
        StringBuilder sb = new StringBuilder();
        sb.append(header+"\n");
        sb.append("\n");
        sb.append("1-Pass en Cours\n");
        sb.append("2-Pass en attente\n");
        sb.append("3-Offres Promo\n");
        sb.append("4-Activer/Desactiver PAYG\n");
       
        sb.append("\n");
        sb.append("Choisissez");
        
        return sb.toString();
    }
    
}
