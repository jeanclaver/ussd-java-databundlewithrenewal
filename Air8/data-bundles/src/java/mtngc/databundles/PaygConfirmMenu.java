/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;
import java.util.Enumeration;

/**
 *
 * @author JcMoutoh
 */
public class PaygConfirmMenu {
    private RequestEnum requestEnum;
    
    public PaygConfirmMenu(RequestEnum requestEnum){
      this.requestEnum=requestEnum;  
    }
    public String getString(){
        StringBuilder sb = new StringBuilder();
        if(requestEnum == RequestEnum.DECOUVRIR){ 
        //sb.append("1 Decouvrir MTN Tarif Xtra\n");
        sb.append("Parlez a seulement 2F/S vers MTN de la 2e a la 5e minute de chaque appel. Bonus a la recharge non applicable\n");           
        sb.append("\n");
        sb.append("\n");
        sb.append("#:Retour");

        } 
        else if(requestEnum == RequestEnum.ACTIVATE){  
        //sb.append("2 Activer le service\n");
        sb.append("Confirmez vous l activation au service MTN KOUDEHI ?\n");           
        sb.append("1 Confirmer\n");
        sb.append("2 Annuler\n");
        sb.append("Repondez");
        }else if(requestEnum == RequestEnum.DEACTIVATE){
        //sb.append("3 Desactiver le service\n");
        sb.append("Voulez-vous desactiver le service MTN KOUDEHI?\n");           
        sb.append("\n");
        sb.append("1 Confirmer\n");
        sb.append("2 Annuler\n");
        sb.append("Repondez");
        }
        String str = sb.toString();
                
        return str;
    }
    
    
}
