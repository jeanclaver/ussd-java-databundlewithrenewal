/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.bundlesdata.renewal.data;

import java.util.Date;

/**
 *
 * @author ekhosa
 */
public class DataBundleRenewal {
    private java.util.Date clearanceDate;
    private Date createdDate;
    private java.sql.Timestamp expiryDate;
    private int MSISDN;
    private int DA;
    private String transactionId;
    
    
    
    /**
     * @return the clearanceDate
     */
    public java.util.Date getClearanceDate() {
        return clearanceDate;
    }

    /**
     * @param clearanceDate the clearanceDate to set
     */
    public void setClearanceDate(java.util.Date clearanceDate) {
        this.clearanceDate = clearanceDate;
    }

    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the expiryDate
     */
    public java.sql.Timestamp getExpiryDate() {
        return expiryDate;
    }

    /**
     * @param expiryDate the expiryDate to set
     */
    public void setExpiryDate(java.sql.Timestamp expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * @return the MSISDN
     */
    public int getMSISDN() {
        return MSISDN;
    }

    /**
     * @param MSISDN the MSISDN to set
     */
    public void setMSISDN(int MSISDN) {
        this.MSISDN = MSISDN;
    }

    /**
     * @return the DA
     */
    public int getDA() {
        return DA;
    }

    /**
     * @param DA the DA to set
     */
    public void setDA(int DA) {
        this.DA = DA;
    }

    /**
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
