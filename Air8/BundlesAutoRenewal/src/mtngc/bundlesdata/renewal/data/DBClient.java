/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.bundlesdata.renewal.data;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import java.util.*;
/**
 *
 * @author JcMoutoh
 */
public class DBClient  {
    
       
    public List<DataBundleRenewal> getTransactionsClearanceList() {
        List<DataBundleRenewal> resp = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();

            
            sb.append("SELECT *  FROM (SELECT tt.*, (CREATE_DATE + VALIDITY) as EXPIRATION_DATE FROM APP_USER.COMBOBUNDLES_TRANSAC tt) t1 ");
            sb.append("where CLEARANCE_DATE is null  ");
            sb.append("and EXPIRATION_DATE <= (sysdate + (60/86400)) ");
          
            sb.append("and EXPIRATION_DATE = ( ");
            sb.append("    SELECT MAX(EXPIRATION_DATE) FROM (SELECT tt.*, (CREATE_DATE + VALIDITY ) as EXPIRATION_DATE FROM APP_USER.COMBOBUNDLES_TRANSAC tt) t2 "); 
            sb.append("    where CLEARANCE_DATE is null ");
            sb.append("    and EXPIRATION_DATE >= trunc(sysdate)  ");
            sb.append("    and t1.MSISDN = t2.MSISDN ");
            sb.append("    and T1.DA = t2.DA  ");      
            sb.append(") ");
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);
            resp = new ArrayList<DataBundleRenewal>();
            while( rs.next() )
            {
                DataBundleRenewal dbt = new DataBundleRenewal();
                int msisdn = rs.getInt("MSISDN"); 
                dbt.setMSISDN(msisdn);
                
                String transactionId = rs.getString("TRANSACTION_ID");
                dbt.setTransactionId(transactionId);
                
                int onNetDA = rs.getInt ("DA"); 
                dbt.setDA(onNetDA);
                
                
                
                Date createdDate = rs.getDate("CREATE_DATE"); 
                dbt.setCreatedDate(createdDate);
                
                java.sql.Timestamp expiryDate = rs.getTimestamp("EXPIRATION_DATE"); 
                dbt.setExpiryDate(expiryDate);                
                
                resp.add(dbt);                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }    
    
    public int updateTransactionClearance (List<DataBundleRenewal> transactions) throws SQLException,ClassNotFoundException,InstantiationException,IllegalAccessException {
        //AuditLog auditLog = null;    
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        String fileName = null;
        
        try{
            conn = getConnection();
            
            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE APP_USER.COMBOBUNDLES_TRANSAC ");                        
            sb.append("SET CLEARANCE_DATE = ? ");
            sb.append("where MSISDN = ? ");
            sb.append("and DA = ? ");
            sb.append("and (CREATE_DATE + VALIDITY) >= trunc(sysdate) ");
            sb.append("and (CREATE_DATE + VALIDITY) < trunc(sysdate+1) ");
            sb.append("AND CLEARANCE_DATE is null ");
            
            
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            conn.setAutoCommit(false);
            
            for(DataBundleRenewal transaction : transactions){
                Date d = transaction.getClearanceDate();
                java.sql.Timestamp t = new java.sql.Timestamp(d.getTime());
                pstmt.setTimestamp(1, t);
                pstmt.setInt (2, transaction.getMSISDN() );
                pstmt.setInt (3, transaction.getDA() );
                pstmt.addBatch();                
            }
            
            // execute insert SQL stetement
            int [] tt = pstmt.executeBatch();
            conn.commit();
            
                  

        }catch(SQLException e){
            
            try{conn.rollback();}catch(Exception f){}
            throw new SQLException(fileName,e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    

    
    



     protected String dbIP = "10.4.0.160" , dbPort = "1521" , dbName = "dbsrver" , dbPass = "app_user" , dbUser = "app_user";
    
    
    
    
    protected Connection getConnection() throws SQLException,ClassNotFoundException,InstantiationException,IllegalAccessException {

        Class.forName("oracle.jdbc.driver.OracleDriver");
        //oracle.jdbc.driver.OracleDriver
        //String driverName = "oracle.jdbc.driver.OracleDriver";
        //Class.forName(driverName).newInstance();
        String serverIP = this.dbIP;
        String portNumber = this.dbPort;
        String sid = this.dbName;
        String url = "jdbc:oracle:thin:@" + serverIP + ":" + portNumber + ":" + sid;
        String username = this.dbUser;
        String password = this.dbPass;
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }   
}
