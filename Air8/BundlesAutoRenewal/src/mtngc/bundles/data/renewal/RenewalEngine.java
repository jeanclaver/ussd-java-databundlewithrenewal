
package mtngc.bundles.data.renewal;
import accesspostgresdb.data.DataClient;
import dbaccess.dbsrver.datareseller.DataDA;
import dbaccess.dbsrver.datareseller.DataPackage;
import java.security.Timestamp;
import java.util.*;
import java.util.Properties;
import mtngc.sms.SMSClient;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import ucipclient.UCIPClientEngine;
import ucipclient.UCIPGetAccountDetailsResponse;
import ucipclient.UCIPUpdateBalanceAndDateResponse;
import ucipclient.UCIPUpdateRefillProfilIDResponse;

/**
 *
 * @author JcMoutoh
 */
public class RenewalEngine {
    
    public void execute(){
        try{
            MLogger.Log(this, LogLevel.ALL, "Starting...... ");
            while(true){

                DataDA client = new DataDA();
                
                MLogger.Log(this, LogLevel.ALL, "Getting renewal from table...");
                List<DataPackage> clist = client.getTransactionsRenewalList();
                MLogger.Log(this, LogLevel.ALL, clist.size()+" renewal records found");
               
                if(clist.size() > 0){
                    List<DataPackage> successlist = new ArrayList<DataPackage>();
                    int n=0;
                    
                    for(DataPackage renew : clist){
                        Date today = new Date();
                         if(renew.getExpiry_date().equals(today.getTime())){
                        n++;
                        renew = renewTransaction(renew);
                        if(renew.getExpiry_date() != null){
                            successlist.add(renew);
                        }
                             
                         }
                        
                    }
                    try{
                        MLogger.Log(this, LogLevel.ALL, "Updating table with "+successlist.size()+" renewed transactions");
                        client.updateTransactionFisrt(successlist);
                    }catch(Exception e){
                        MLogger.Log(this, e);
                    }
                }
                MLogger.Log(this, LogLevel.ALL, "Getting already renewal from table...");
                List<DataPackage> clistrenewal = client.getTransactionsAlreadyRenewalList();
                MLogger.Log(this, LogLevel.ALL, clist.size()+" renewal records found");
                if(clistrenewal.size() > 0){
                    List<DataPackage> successlistrenew = new ArrayList<DataPackage>();
                    int n=0;
                    for(DataPackage renewal : clistrenewal){
                        Date today = new Date();
                        if(renewal.getExpiry_date().equals(today.getTime())){
                          n++;
                          renewal = renewTransaction(renewal);
                          if(renewal.getExpiry_date() != null){
                               successlistrenew.add(renewal);
                              
                          }
                            
                        }
                        
                          
                    }
                     try{
                        MLogger.Log(this, LogLevel.ALL, "Updating table with "+successlistrenew.size()+" renewed transactions");
                        client.updateRenewalContinuous(successlistrenew);
                    }catch(Exception e){
                        MLogger.Log(this, e);
                    }
                        
                }
                
                    
                try{
                   long sleepTime = 1000;
//                 long sleepTime = 24 * 3600 * 1000;
                 MLogger.Log(this, LogLevel.ALL, "Sleeping for "+sleepTime+" milliseconds");
                Thread.sleep(sleepTime);//  
                }catch(Exception e){
                    MLogger.Log(this, e);
                }


            }
        }catch(Exception e){
           MLogger.Log(this, e);
        }
        
    }  
    public DataPackage renewTransaction(DataPackage transaction){
        
          
        try { 
            
            StringBuilder sb = new StringBuilder();
            DataDA orcl = new DataDA();
            DataClient pg = new DataClient();
            String refillExternalData = "DATA_BUNDLES";
            UCIPClientEngine ucipEngine = new UCIPClientEngine();
            String msisdn = transaction.getCustomer_msisdn();
            String transactionId = transaction.getTransaction_id();
            UCIPGetAccountDetailsResponse getAccountResponse = ucipEngine.GetAccountDetails(msisdn, transactionId);
             if(getAccountResponse != null){
                 int sc = getAccountResponse.getServiceClass();
                 MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|Service Class="+sc);
                 double balance = getAccountResponse.getBalance();
                int price = transaction.getPrice();
                if(price > balance){
                   MLogger.Log(this, LogLevel.ALL,  "Subscriber:"+msisdn+"  |Cher client vous n avez pas suffisamment de credit pour acheter ce forfait! Merci de votre fidelite"+sc);  
                }else{
                    MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+transaction.getPackageName()+"|Price: "+transaction.getPrice()+"|Funds are sufficient");
                    UCIPUpdateBalanceAndDateResponse deductMAResponse = ucipEngine.UpdateMainAccountBalanceAndDate(msisdn, transactionId, -price, true, new Date(), refillExternalData); 
                    int x = deductMAResponse.getResponseCode();
                    if((x == 0) && (!deductMAResponse.isError())){
                        double balanceAfter = deductMAResponse.getBalance();
                        MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+transaction.getResult()+"|Price: "+transaction.getPrice()+"|New Balance: "+balanceAfter+"|Success");
                        UCIPUpdateRefillProfilIDResponse refillDAResponse = new UCIPUpdateRefillProfilIDResponse();
                        refillDAResponse = ucipEngine.RefillProfileID(msisdn, transactionId,  transaction.getKeyword(), refillExternalData);
                       MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+transactionId+"|"+transaction.getPackageType()+"|"+transaction.getKeyword()+"| Sending UCIP Request.");
                       int y = refillDAResponse.getResponseCode();  
                        if((y == 0) && (!refillDAResponse.isError())){
                             MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|externalData Response: "+refillExternalData);
                             MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|Refill Profil ResponseCode: "+refillDAResponse.getResponseCode());
                             SmsNotifcation(msisdn);
                             SendPOSSuccessSMS(msisdn, transaction.getPrice(), transaction.getPackageName());
                             MLogger.Log(this, LogLevel.ALL, "Cher client votre demande d achat internet a ete envoyee, vous recevrez un message de confirmation !!! Merci de votre fidelite\n");
                             MLogger.Log(this, LogLevel.DEBUG, " Subscriber: "+msisdn+"|Session "+transactionId+"|Package Name: "+transaction.getPackageName()+"| Package Type:"+transaction.getPackageType()+"|Package Keyword: "+transaction.getKeyword()+"|Result:"+transaction.getResult() );
                                                          
                        // Insertion in the DB Postgres
                        
                        // Insertion in the DB Oracle
                        
                        }else{
                            UCIPUpdateBalanceAndDateResponse refillMAResponse = ucipEngine.UpdateMainAccountBalanceAndDate(msisdn, transactionId, +price, true, new Date(), refillExternalData);
                             if((x == 0) && (!refillMAResponse.isError())){
                             MLogger.Log(this, LogLevel.DEBUG, " Subscriber: "+msisdn+"|Session "+transactionId+"|Package Name: "+transaction.getPackageName()+"| Package Type:"+transaction.getPackageType()+"|Package Keyword: "+transaction.getKeyword()+"|Response:"+refillMAResponse.getResponseCode() );
                            
//                             transaction.setExpiry_date((Timestamp) new java.util.Date());
                             }
                        }
                    }
                }
             }else{
                MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|error:"+getAccountResponse);
                MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId);
             }
                
           
        } catch (Exception e) {
                MLogger.Log(this, e);
        }
        return transaction ;      
    
    }
        
    
    
    
    private String loadProperty(Properties properties, String name){
        String value = properties.getProperty(name);
        if(name == null)
            throw new RuntimeException(name+" property is missing");
        
        return value;
    }
    
     private void SmsNotifcation(String msisdn){
        String msisdnStr = msisdn;
        msisdnStr = "224" + msisdnStr;// "224664222544";
        String msg =  "MTN Yello TV! Plus 500 films & Series, + 20 chaines de Télé EN DIRECT sur votre smartphone et SANS CONSOMMER VOTRE PASS! Telechargez sur bit.ly/39Xwnu6  .";
        SMSClient smsClient = new SMSClient();
        smsClient.sendSMS("Internet", msisdnStr, msg);
        
    }
     private void SendPOSSuccessSMS(String msisdn, int price, String bundle){
        String msg = "Vous avez ete debite de  "+price+" FG pour le renouvellement  de votre connexion Internet de "+bundle+" . Tapez *223# pour le solde.";
        String msisdnStr = msisdn;
        //msisdn = "664222544";
        if(!msisdnStr.startsWith("224")){
            msisdnStr = "224" + msisdnStr;
        } 
        
        SMSClient smsClient = new SMSClient();
        smsClient.sendSMS("Internet", msisdnStr, msg);
        
    }  
}
