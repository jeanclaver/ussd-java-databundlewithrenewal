/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;
import java.util.ArrayList;
//import mtn.gn.com.data.Bundle;
//import mtn.gn.com.data.DataDA;

import java.util.Enumeration;
import java.util.List;
//import mtngc.bundles.core.UserOption;

/**
 *
 * @author JcMoutoh
 */
public class MomoSUBMenu {
    private RequestEnum requestEnum;
    
    
    public MomoSUBMenu(RequestEnum requestEnum){
        this.requestEnum=requestEnum;
    }
    
    

    public String getString(){
        StringBuilder sb = new StringBuilder(); 
        
        if(requestEnum == RequestEnum.MOMOPASSJOUR){
            sb.append("BONUS PASS MoMo 24h\n");
            sb.append("1. 40MB + 10MB a 1000F\n");
            sb.append("2. 120MB + 30MB a 2000F\n");
            sb.append("3. 250MB + 125MB a 3000F\n");
            sb.append("4. 500MB + 250MB a 5000F\n");

            sb.append("# Retour\n");
            sb.append("Repondez");
        }else if(requestEnum == RequestEnum.MOMOPASS48H){
            sb.append("BONUS PASS MoMo 48h\n");
            sb.append("1. 60MB + 15MB a 1500F\n");
            sb.append("2. 140MB + 35MB a 2500F\n");
            sb.append("3. 300MB + 150MB a 4000F\n");
            sb.append("4. 500MB + 250MB a 5000F\n");
            sb.append("# Retour\n");
            sb.append("Repondez");
        }
        else if(requestEnum == RequestEnum.MOMOPASSWEEK){
            sb.append("BONUS PASS MoMo Semaine\n");
            sb.append("1. 650MB + 375MB a 10000F\n");
            sb.append("2. 1.3GB + 650MB a 15000F\n");
            sb.append("3. 2.5GB + 1.25GB a 30000F\n");
//            sb.append("4. 500MB + 250MB a 5000F\n");
            sb.append("# Retour\n");
            sb.append("Repondez");
        }
         else if(requestEnum == RequestEnum.MOMOPASSMOIS){
            sb.append("BONUS PASS MoMo Mois\n");
            sb.append("1. 2.5GB + 1.25GB a 40000F\n");
            sb.append("2. 5GB + 2.5GB a 75000F\n");
            sb.append("3. 7.5GB + 3.75GB a 110000F\n");
            sb.append("4. 15GB + 7.5GB a 190000F\n");
            sb.append("5. 30GB + 15GB a 355000F\n");
            sb.append("6. 50GB + 25GB a 590000F\n");
            sb.append("7. 100GB + 50GB a 1100000F\n");
            
            sb.append("# Retour\n");
            sb.append("Repondez");
        }
        String str = sb.toString();
                
        return str;
    }

   
    
    

    
}
