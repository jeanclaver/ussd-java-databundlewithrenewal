/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;
import java.io.Serializable;
import java.util.*;
import dbaccess.dbsrver.datareseller.*;
import dbaccess.dbsrver.GoodYearMsisdn;
import dbaccess.dbsrver.datareseller.DataDA;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
/**
 *
 * @author JcMoutoh
 */
public class MomoPackageNameMenu  {
    String header;
    protected String msg;    
    public static HashMap MOMOPACKAGE_NAMES = new HashMap();
    private String packageType;
    private final static Object _lock = new Object(); 
    private DataPackage packagesAddon[];
    
    
    public MomoPackageNameMenu(String msisdnStr, String packageType)  {
       init( msisdnStr,packageType);
    }
    
    protected void init(String msisdnStr, String packageType) {
        this.packageType = packageType;
        header ="BONUS Pass Momo";
        
        DataDA dbClient = new DataDA();
        try{
            
            int msisdn = Integer.parseInt(msisdnStr);
            
            Object obj = MOMOPACKAGE_NAMES.get(packageType);
            if(obj == null){
                synchronized(_lock){
                    obj = MOMOPACKAGE_NAMES.get(packageType);
                    if(obj == null){
                        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Fetching package name "+packageType);


                       List<DataPackage> packageNames = dbClient.getMomoPackages(packageType);

                        if ((packageNames != null) && (packageNames.size() > 0)){
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|"+packageType+" successfully loaded|Size = "+packageNames.size());

                            MOMOPACKAGE_NAMES.put(packageType, packageNames);
                        }else {               

                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Package Name:"+packageType+"|No packages returned");
                        }
                    }
                }//
            }
            
        }catch (Exception e) {
               MLogger.Log(this, e);
        }    
    }
    
    
    public String getString(){
        StringBuilder sb = new StringBuilder();
        sb.append(header+"\n");
        Object obj = MOMOPACKAGE_NAMES.get(this.packageType);
        if(obj != null){
            List<DataPackage> packages = (List<DataPackage>)obj;

             packagesAddon = new DataPackage[packages.size()];
            int i=1;
            int j=0;
            for(DataPackage momopackage : packages){
               
               sb.append(i+". "+momopackage.getPackageName()+" a "+momopackage.getPrice()+"F\n"); 
              
               
               DataPackage currentPackage = new DataPackage();
               currentPackage.setAddonprice(momopackage.getAddonprice());
               currentPackage.setPackageName(momopackage.getPackageName());
                packagesAddon [j]= currentPackage;
                i++;
                j++;
            }  
            
            sb.append("# Retour  \n"); 

            sb.append("\n");
            sb.append("Repondez");
        }else {
            sb.append("Pardon. Nous avons rencontre une erreur...\n");
            sb.append("\n");
        }
        return sb.toString();
    }
    
    public DataPackage getMomoPassPackage(String packageType, int index){
        
        
        Object obj = MOMOPACKAGE_NAMES.get(this.packageType);
        if(obj != null){
            List<DataPackage> packages = (List<DataPackage>)obj;
            int i=1;
            for(DataPackage momopackage : packages){
                if(i == index)
                    return momopackage;
                i++;
            } 
        }
         
        return null;
    }

    
//     public DataPackage getPackageByPackagetypeAndOrder(String  packageType, int order){
//         
//          DataDA dbClient = new DataDA();
//          DataPackage dataAdon = dbClient.getAddonByPackageTypeAndPackageOrder(packageType, order);
//          return dataAdon;
//     }
    
    
   
    
}
