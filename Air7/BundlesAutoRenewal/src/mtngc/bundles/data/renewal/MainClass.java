/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.bundles.data.renewal;

import java.io.File;
import java.util.ArrayList;
import mtnz.util.logging.MLogger;

/**
 *
 * @author JcMoutoh
 */
public class MainClass {
    
   static String ContextKeyName;
    static ArrayList <Integer> supportedMSISDNs;
    static
    {  
        ContextKeyName = "data-renewal";
        if (System.getProperty("os.name").toLowerCase().contains("windows")){
        String filePath = "C:\\Logs\\"+ContextKeyName+"_v1";
        try{
        File file = new File(filePath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        MLogger.setHomeDirectory(filePath);
        }
        else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
       String filePath = "/Users/macbook/logs/ussd_"+ContextKeyName+"_v1";
        try{
        File file = new File(filePath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        MLogger.setHomeDirectory(filePath);
            
            
        }
        else  {
        String filePath = "/var/log/ussd_"+ContextKeyName+"_v1";
          try{
        File file = new File(filePath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
           MLogger.setHomeDirectory(filePath); 
        }
        
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new RenewalEngine().execute();
    }
    
}
